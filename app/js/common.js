$(function() {

	$('#my-menu').mmenu({

		extensions: ['theme-black', 'effect-menu-slide', 'pagedim-black', ],
		navbar: {
			title: '<img src="/img/logo-1.svg" alt="Салон Красоты S&Mitler"'
		},
		offCanvas: {
			position: 'right'
		}
	});

	var api = $('#my-menu').data('mmenu');
	api.bind('open:finish', function() {
		$('.hamburger').addClass('is-active');})
		.bind('close:finish', function(){
			$('.hamburger').removeClass('is-active')
		});
	$('.carousel-services').on('initialized.owl.carousel', function () {
		setTimeout(function () {
			carouselService();
		}, 100)

	})
	$('.carousel-services').owlCarousel({
		loop:true,
		nav: true,
		dost: false,
		smartSpeed: 700,
		navText: ['<i class="fas fa-angle-double-left"></i>', '<i class="fas fa-angle-double-right"></i>'],
		responsiveClass: true,
		callbacks: true,
		onInitialize: function() {
			carouselService();
		},
		onRefresh: function() {
			carouselService();
		},
		responsive: {
			0: {
				items: 1
			},
			800: {
				items: 2
			},
			1100: {
				items: 3
			}
		}
	});




	
	// function carouselService() {
	// 	$('.carousel-services-item').each(function () {
	// 		let ths = $(this),
	// 			thsh = ths.find('.carousel-services-content').outerHeight();
	// 			console.log(ths.find('.carousel-services-img'));
	// 			ths.find('.carousel-services-img').css('min-height', thsh);
	//
	// 	})
	// }
	function carouselService() {
		document.querySelectorAll('.carousel-services-item').forEach(item => {
			const thsh = item.querySelector('.carousel-services-content').clientHeight;
			item.querySelector('.carousel-services-img').style.minHeight = `${thsh}px`
		})
	}
	function transformTitles() {
		document
			.querySelectorAll('.carousel-services-composition .h3')
			.forEach(item => {
				item.innerHTML = item.innerHTML.replace(/(\S+)\s*$/, '<span>$1</span>');
			});
	}transformTitles();


	function transformTitles2() {
		document
			.querySelectorAll('section .h2')
			.forEach(item => {
				item.innerHTML = item.innerHTML.replace(/^(\S+)/, '<span>$1</span>');
			});
	}transformTitles2();
	// transformTitles();

	// carouselService();

	// $('.carousel-services-composition .h3').each(function (){
	// 	var item = $(this);
	// 	item.html(item.html().replace(/(\S+)\s*$/, '<span>$1</span>'));
	// })
	/// Разобрать на нативный JS

	function onResize(){
		$('.carousel-services-content').equalHeights();
	}onResize();
	window.onresize = function () {
		onResize();
	}




});
